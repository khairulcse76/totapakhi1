
@extends('layouts.app')
@section('content')
    @include('includes.massage-block')
    <section class="row new-post">
        <div class="col-md-6 col-md-offset-3">
            <header><h3>What do you want to sey.....</h3></header>
            <form action="{{ route('post.create') }}" method="Post">
                {{ csrf_field() }}
                <div class="form-group">
                    <textarea class="form-control" name="body" id="new-post" rows="5" placeholder="Write somthing"></textarea>
                    @if ($errors->has('body'))
                        <span style="color: red">
                            <strong>{{ $errors->first('body') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Create Post</button>
                <input type="hidden" value="{{ Session::token()}}" name="_token">
            </form>
        </div>
    </section>

    <section class="row posts">
        <div class="col-md-6 col-md-offset-3">
            <header><h3>What other people sey.</h3></header>

            @foreach($posts as $post)
                <article class="post">
                    <div class=" header text-info btn-success">{{ $post->user->name }}</div>
                    <p>{{ $post->body }}</p>
                    <div class="info">
                        posted by {{ $post->user->name }}  on {{ $post->created_at }}
                    </div>
                    <div class="interaction">
                        <a href="#">Like</a>
                        <a href="#">Dislike</a>
                        <a href="#">Edit</a>
                        <a href="{{ route('post.delete', ['post_id'=>$post->id]) }}">Delete</a>
                    </div>
                </article>
            @endforeach

        </div>
    </section>
@endsection