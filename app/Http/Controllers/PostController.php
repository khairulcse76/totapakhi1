<?php

namespace App\Http\Controllers;
use App\Http\Requests\PostReauest;
use App\Http\Requests\PostRequest;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function getHome()
    {
        $posts=Post::orderBy('created_at', 'desc')->get();
        return view('home', ['posts'=>$posts]);
    }
    public function postCreatePost(PostReauest $request)
    {
        $post = new Post();
        $post->body = $request['body'];
        if($request->user()->posts()->save($post))
        {
            $massage='Post Successfully Created....!';
        }else{
            $massage='There was an error....!';
        }
        return redirect()->route('home')->with(['massage'=>$massage]);
    }

    public function getDeletePost($post_id){
        $post=Post::where('id', $post_id)->first();
        $post->Delete();
        return redirect()->route('home')->with(['massage'=>'Successfully Delete']);
    }

}